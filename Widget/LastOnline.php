<?php
namespace AnzahTools\LastMemberOnline\Widget;

use XF\Widget\AbstractWidget;

/**
 * Class LastOnline
 *
 * @package AnzahTools\LastMemberOnline\Widget
 */
class LastOnline extends AbstractWidget
{
	protected $defaultOptions = [
		'at_lmo_time' => '24',
		'at_lmo_sort' => 'last_activity',
		'at_lmo_order' => 'DESC',
		'at_lmo_usercount' => '0',
		'at_lmo_limit' => '50'
	];

	public function render()
	{

		if (!\XF::visitor()->canViewMemberList())
		{
			return '';
		}

		$options = $this->options;
		$at_lmo_time = $options['at_lmo_time'];
		$at_lmo_sort = $options['at_lmo_sort'];
		$at_lmo_order = $options['at_lmo_order'];
		$at_lmo_usercount = $options['at_lmo_usercount'];
		$at_lmo_limit = $options['at_lmo_limit'];

		if (!\XF::visitor()->canbypassUserPrivacy())
		{
			$users = \XF::finder('XF:USER')->where([
  				['last_activity', '>=' ,time() - 3600 * $at_lmo_time],
					['visible', '=' , '1']
  			])->order($at_lmo_sort,$at_lmo_order)->limit($at_lmo_limit)->fetch();
	 } else {
		 $users = \XF::finder('XF:USER')->where([
 				['last_activity', '>=' ,time() - 3600 * $at_lmo_time]
 			])->order($at_lmo_sort,$at_lmo_order)->limit($at_lmo_limit)->fetch();
	 }
		$outputUsers = [];

		foreach($users as $user)
		{
			array_push($outputUsers,[
				'user_id' => $user['user_id'],
				'username' => $user['username'],
				'visible' => $user['visible'],
				'display_style_group_id' => $user['display_style_group_id']
			]);
		}

		$at_lmo_usercounted = count($users);
		$at_lmo_usercounted_more = $at_lmo_usercounted - $at_lmo_limit;

		$viewParams = [
				'outputUsers' => $outputUsers,
				'at_lmo_usercounted' => $at_lmo_usercounted,
				'at_lmo_usercounted_more' => $at_lmo_usercounted_more

		];

		return $this->renderer('at_lmo_widget',$viewParams);
	}

	public function verifyOptions(\XF\Http\Request $request, array &$options, &$error = null)
	{
		$options = $request->filter([
			'at_lmo_time' => 'str',
			'at_lmo_sort' => 'str',
			'at_lmo_order' => 'str',
			'at_lmo_usercount' => 'bool',
			'at_lmo_limit' => 'str'
		]);
		return true;
	}
}
